if ! [ -f ~/.zplug/zplug ]; then
    curl -fLo ~/.zplug/zplug --create-dirs https://git.io/zplug
    source ~/.zplug/zplug && zplug update --self
fi

ZSH_TMUX_AUTOSTART=true # can be overriden in ~/.zshrc-pre.local

[ -s ~/.zshrc-pre.local ] && source ~/.zshrc-pre.local

source ~/.zplug/zplug

zplug "plugins/git", from:oh-my-zsh
zplug "plugins/gitfast", from:oh-my-zsh
zplug "plugins/git-extras", from:oh-my-zsh
zplug "plugins/github", from:oh-my-zsh

zplug "plugins/jsontools", from:oh-my-zsh

zplug "plugins/command-not-found", from:oh-my-zsh
zplug "plugins/autojump", from:oh-my-zsh
zplug "plugins/common-aliases", from:oh-my-zsh
zplug "plugins/dircycle", from:oh-my-zsh
zplug "plugins/dirhistory", from:oh-my-zsh

zplug "plugins/lein", from:oh-my-zsh
zplug "plugins/sbt", from:oh-my-zsh
zplug "plugins/scala", from:oh-my-zsh
zplug "plugins/cabal", from:oh-my-zsh
zplug "plugins/python", from:oh-my-zsh
zplug "plugins/ruby", from:oh-my-zsh
zplug "plugins/pip", from:oh-my-zsh
zplug "plugins/coffee", from:oh-my-zsh

zplug "plugins/web-search", from:oh-my-zsh

zplug "plugins/fasd", from:oh-my-zsh

zplug "zsh-users/zsh-history-substring-search"

zplug "plugins/tmux", from:oh-my-zsh

# fix for using zsh inside emacs
if [ -n "$INSIDE_EMACS" ]; then
    zplug "themes/robbyrussell", from:oh-my-zsh
else
    zplug "mafredri/zsh-async"
    zplug "sindresorhus/pure"
    # zplug "nojhan/liquidprompt", from:oh-my-zsh
    zplug "zsh-users/zsh-syntax-highlighting"
fi

if ! zplug check; then
    zplug install
fi

zplug load

source ~/seartipy/dotfiles/shellrc

[ -s ~/.zshrc-post.local ] && source ~/.zshrc-post.local
